package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.mvc.WebSocket;
import play.libs.Json;
import potentialgames.controller.GameController;
import potentialgames.data.ICell;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 28.11.13
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
public class MADNObserver implements Observer {

    private GameController gc;
    private WebSocket.Out<String> out;

    public MADNObserver(GameController gc, WebSocket.Out<String> out){
        this.gc = gc;
        this.out = out;
        gc.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        ObjectNode result = new Json().newObject();
        int[] pinsInHomes = new int[gc.getPlayercount()];
        int[] pinsInTargets = new int[gc.getPlayercount()];
        for(int i = 0; i < gc.getPlayercount(); i++){
            pinsInHomes[i] = gc.getField().getHome(i).size();
            pinsInTargets[i] = gc.getField().getTarget(i).size();
        }
        List<ICell> way = new ArrayList<>();
        result.put("homes", Json.toJson(pinsInHomes));
        result.put("way", Json.toJson(gc.getWay()));
        result.put("targets", Json.toJson(pinsInTargets));
        result.put("winstatus", gc.getWin());
        result.put("amzug", gc.getAmZug());
        out.write(result.toString());
    }
}
