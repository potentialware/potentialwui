package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


import models.impl.*;
import play.data.*;
import static play.data.Form.*;
import play.libs.F;
import play.libs.Json;
import play.libs.OpenID;
import play.mvc.*;
import potentialgames.Potentialgames;
import potentialgames.controller.GameController;
import potentialgames.data.ICell;
import views.html.contact;

import play.libs.OAuth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.google.api.services.plus.Plus;
//import com.google.api.services.plus.model.PeopleFeed;

public class MainController extends Controller {

    static GameController gc;

    static {
        gc = Potentialgames.getInstance().getController();
    }

    public MainController() {
        gc.gameInit();
    }
    @Security.Authenticated(Secured.class)
    public static Result index() {
        return ok(views.html.index.render("Lets play WEBMADN", gc, session()));
    }

    public static Result contact() {
        return ok(contact.render("MADN - contact", session()));
    }

    public static Result restart() {
        gc.gameInit();
        return ok(views.html.index.render("Lets play WEBMADN - again...", gc, session()));
    }

    public static Result login() {
        return ok( views.html.login.render("WEBMADN - Login", form(Login.class)));
    }


    public static Result logout() {
        session().clear();
        return redirect(routes.MainController.index());
    }

    public static Result authenticate() {
        Form<Login> loginform = Form.form(Login.class).bindFromRequest("email", "password");


        if (loginform.hasErrors()) {
            return badRequest(views.html.login.render("MADN login failed", loginform));
        } else {
            session().clear();
            User user = loginform.get().get();
            session("email", loginform.get().email);
            session("name", user.name);
            session("img", routes.Assets.at("images/profiles/" + user.imgurl) + "");
            return redirect(
                    routes.MainController.index()
            );
        }
    }

    public static class Secured extends Security.Authenticator {

        @Override
        public String getUsername(Http.Context ctx) {
            return ctx.session().get("email");
        }

        @Override
        public Result onUnauthorized(Http.Context ctx) {
            return redirect(routes.MainController.login());
        }
    }

    public static Result commandline(String command) {
        Potentialgames.getInstance().getTui().tuihandler(command);
        return ok(views.html.index.render("Got your command " + command, gc, session()));
    }

    public static Result authOpenId(String provider){
        switch(provider){
            case "google":
            String providerURL = "https://www.google.com/accounts/o8/id";
            String returnToUrl = "http://webmadn.herokuapp.com/openID/verify";
            HashMap<String, String> attributes = new HashMap();
            attributes.put("Email", "http://schema.openid.net/contact/email") ;
            attributes.put("FirstName", "http://schema.openid.net/namePerson/first") ;
            attributes.put("LastName", "http://schema.openid.net/namePerson/last") ;
            attributes.put("uid", "http://schemas.openid.net/ax/api/user_id");
            attributes.put("imageurl", "http://schemas.openid.net/ax/api/picture");
            F.Promise<String> redirectUrl = OpenID.redirectURL(providerURL, returnToUrl, attributes);
            return redirect(redirectUrl.get());

        }

        return redirect(routes.MainController.index());
    }

    public Result verify(){
        F.Promise<OpenID.UserInfo> userInfoPromise = OpenID.verifiedId();
        OpenID.UserInfo userInfo = userInfoPromise.get();

        session().clear();
        session("email", userInfo.attributes.get("Email"));
        session("name", userInfo.attributes.get("FirstName") + " " + userInfo.attributes.get("LastName"));
        session("img" , "https://plus.google.com/s2/photos/profile/" +  userInfo.attributes.get("uid") + "?sz=500");
        return redirect(
                routes.MainController.index()
        );
    }




    public static Result ajax(String command) {
        ObjectNode result = new Json().newObject();
        switch (command) {
            case "roll":
                int roll = gc.roll();
                if (roll == gc.ERR_DOUBLE_ROLL) {
                    result.put("status", "error");
                    result.put("message", "Double rolling not permitted!");
                    return badRequest(result);
                } else {
                    result.put("roll", roll);
                    result.put("player", gc.getAmZug());
                }
                break;
            case "next":
                gc.next();
                result.put("player", gc.getAmZug());
                break;
            default:
                result.put("status", "error");
                result.put("message", "bad ajax request");
                return badRequest(result);
        }
        return ok(result);


    }

    public static Result postJson() {
        ObjectNode result = new Json().newObject();
        JsonNode json = request().body().asJson();
        String action = json.findPath("action").textValue();
        String status = "";
        switch (action) {
            case "entry":
                int entryStatus;
                entryStatus = gc.pinEntry();
                if (entryStatus == GameController.ERR_NO_ROLLED_SIX_TO_ENTRY) {
                    result.put("status", "error");
                    result.put("message", "No six rolled");
                    return badRequest(result);
                } else if (entryStatus == GameController.ERR_NO_PINS_IN_HOME) {
                    result.put("status", "error");
                    result.put("message", "You have no pins in your home");
                    return badRequest(result);
                } else if (entryStatus == GameController.ERR_SAME_PLAYER_ON_FIELD) {
                    result.put("status", "error");
                    result.put("message", "there is your own pin on your entry field!");
                    return badRequest(result);
                } else if (entryStatus == 0) {
                    result.put("status", "ok");
                    result.put("message", "pin entered");
                }
                break;
            case "move":
                int moveStatus;
                moveStatus = gc.pinMove(json.findPath("pinnr").intValue());
                if (moveStatus == GameController.ERR_PIN_NOT_ON_FIELD) {
                    result.put("status", "error");
                    result.put("message", "this pin is not on the gamefield, if have no idea how you can do this magic stuff?!?!");
                    return badRequest(result);
                } else if (moveStatus == GameController.ERR_SAME_PLAYER_ON_FIELD) {
                    result.put("status", "error");
                    result.put("message", "there is your own pin onthe target field!");
                    return badRequest(result);
                } else if(moveStatus == GameController.ERR_NOT_ROLLED){
                    result.put("status", "error");
                    result.put("message", "please roll before move!");
                    return badRequest(result);
                } else if (moveStatus == 0) {
                    result.put("status", "ok");
                    result.put("message", "pin moved");
                }
                break;
            case "options":
                try {
                    int pincount = json.findPath("iPinCount").asInt();
                    int playercount = json.findPath("iPlayerCount").asInt();
                    int fieldlength = json.findPath("iFieldLength").asInt();
                    gc.setPincount(pincount);
                    gc.setPlayercount(playercount);
                    gc.setFields(fieldlength);
                    gc.gameInit();
                    result.put("status", "ok");
                    result.put("message", "options set");
                    return ok(result);
                } catch (Exception e) {
                    result.put("status", "error");
                    result.put("message", "error while setting up options, please retry");
                    return badRequest(result);
                }
            default:
                result.put("status", "error");
                result.put("message", "bad ajax request");
                return badRequest(result);
        }
        result.put("status", status);
        return ok(result);
    }

    public static Result redraw() {
        ObjectNode result = new Json().newObject();
        int[] pinsInHomes = new int[gc.getPlayercount()];
        int[] pinsInTargets = new int[gc.getPlayercount()];
        for (int i = 0; i < gc.getPlayercount(); i++) {
            pinsInHomes[i] = gc.getField().getHome(i).size();
            pinsInTargets[i] = gc.getField().getTarget(i).size();
        }
        List<ICell> way = new ArrayList<>();
        result.put("roll", gc.getSteps());
        result.put("homes", Json.toJson(pinsInHomes));
        result.put("way", Json.toJson(gc.getWay()));
        result.put("targets", Json.toJson(pinsInTargets));
        result.put("winstatus", gc.getWin());
        result.put("player", gc.getAmZug());
        return ok(result);
    }

    public static WebSocket<String> socket() {
        return new WebSocket<String>() {
            @Override
            public void onReady(In<String> in, Out<String> out) {
                new MADNObserver(gc, out);
            }
        };
    }


}
