package models.impl;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 09.01.14
 * Time: 11:07
 * To change this template use File | Settings | File Templates.
 */
public class User {
    public String email;
    public String password;
    public String name;
    public String imgurl;

    public User(){};
    public User(String m, String p){
        email = m;
        password = p;
    }

    public User(String m, String p, String name, String img){
        email = m;
        password = p;
        this.name = name;
        imgurl = img;
    }
}
