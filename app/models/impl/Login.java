package models.impl;


import models.aLogin;
import models.impl.User;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 09.01.14
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public class Login extends aLogin {

    protected User user;

    public Login(){
        this.userlist = new Users();
    }
    public String validate() {
        User tmp = this.authenticate(new User(this.email, this.password));
        if (tmp == null) {
            return "Invalid user or password";
        }
        this.user = tmp;
        return null;
    }


    private User authenticate(User user){
        User wantsToLogin = userlist.getUser(user.email);
        if(wantsToLogin == null)
            return null;

        if(wantsToLogin.password.equals(user.password)) {
            user = wantsToLogin;
            return wantsToLogin;

        }

        return null;
    }

    public User get(){
        return user;
    }


}