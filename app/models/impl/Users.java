package models.impl;

import models.iUsers;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 09.01.14
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
public class Users implements iUsers {
    public List<User> userlist= new LinkedList<>();
    public Users(){
        User u1 = new User("test@test.de", "blubb123", "Student with headcrab", "Headcrab.jpg");
        userlist.add(u1);
    }
    @Override
    public User getUser(String username){
        User retUser = null;
        for(User x: userlist){
            if(x.email.equals(username)){
                retUser = x;
                break;
            }
        }
        return retUser;
    }
}
