package models;

import models.impl.User;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 21.01.14
 * Time: 18:28
 * To change this template use File | Settings | File Templates.
 */
public interface iUsers {
    User getUser(String username);
}
