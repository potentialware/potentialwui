$(document).ready(function ($) {
    setPos();
    connect();
});
$('#roll').on('click', function (event) {
    event.preventDefault();
    $("#statusmsg").html("");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/ajax/roll",
        error: function (errmsg) {
            $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
        },
        failure: function (errmsg) {
            $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
        }
    })
});
$('#next').on('click', function (event) {
    event.preventDefault();
    $("#statusmsg").html("");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/ajax/next",
        error: function (errmsg) {
            $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
        },
        failure: function (errmsg) {
            $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
        }
    })
});
$('.field').on('click', function (event) {

    event.preventDefault();
    pinnr = event.target.innerHTML.trim().substr(event.target.innerHTML.trim().length - 1, 1)
    player = event.target.id.substr(event.target.id.length - 1, 1);
    fieldtype = event.target.className.split(" ");
    if ($.inArray("home", fieldtype) > -1) {
        json = [
            {"action": "entry", "player": player}
        ];
        json = JSON.stringify(json)
        $.ajax({
            type: "POST",
            dataType: "json",
            data: json,
            contentType: "application/json; charset=utf-8",
            url: "/postjson",
            error: function (errmsg) {
                $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
            },
            failure: function (errmsg) {
                $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
            }
        })
    }
    if ($.inArray("gamefield", fieldtype) > -1) {
        json = [
            {"action": "move", "player": player, "pinnr": pinnr - 1}
        ];
        json = JSON.stringify(json)
        $.ajax({
            type: "POST",
            dataType: "json",
            data: json,
            contentType: "application/json; charset=utf-8",
            url: "/postjson",
            error: function (errmsg) {
                $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
            },
            failure: function (errmsg) {
                $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
            }
        })
    }
});

function connect() {
    var socket = new WebSocket("ws://" + window.document.location.host + "/socket");
    message('Socket Status: ' + socket.readyState + ' (ready)');

    socket.onopen = function () {
        message('Socket Status: ' + socket.readyState + ' (open)');
    };

    socket.onmessage = function (msg) {
        var msg = JSON.parse(msg.data);
        redraw()
    };

    socket.onclose = function () {
        message('Socket Status: ' + socket.readyState + ' (Closed)');
        connect();
    };

    function send() {
    }

    function message(msg) {
        console.log(msg);
    }


}//End connect

function redraw() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/redraw",
        success: function (data) {
            $('#activeplayer').html("Player " + (data.player + 1) + " turn");
            $('#dicefield').html("You rolled : " + data.roll);
            var i;
            for (i = 0; i < data.homes.length; i++) {
                $(".home#fieldplayer" + i).html(data.homes[i]);
            }

            for (i = 0; i < data.way.length; i++) {
                if (data.way[i].pin != null) {
                    $(".way" + i).html("Pin " + (data.way[i].pin.pinNr + 1));
                    $(".way" + i).attr("class", "btn way" + i + "  field gamefield");
                    $(".way" + i).attr("id", "fieldplayer" + data.way[i].player.playerNr);
                } else {
                    $(".way" + i).html("#");
                    $(".way" + i).attr("class", "btn btn-danger way" + i + " gamefield field disabled");
                    $(".way" + i).attr("id", i);
                }
            }

            for (i = 0; i < data.targets.length; i++) {
                $(".targets#fieldplayer" + i).html(data.targets[i]);
            }
            if (data.winstatus) {
                //TODO modal WIN
                $("#winmsg").html("Player " + (data.player + 1) + " won the game!");
                $("#winmsg").removeClass('hidden');
                $("#roll").addClass("disabled");
                $("#next").addClass("disabled");
            } else {
                $("#winmsg").html("");
                $("#winmsg").addClass('hidden')
                $("#roll").removeClass("disabled");
                $("#next").removeClass("disabled");
            }
            //setTimeout(function(){redraw();}, 5000);

        },
        error: function (errmsg) {
            $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
        },
        failure: function (errmsg) {
            $("#statusmsg").html(JSON.parse(errmsg.responseText).message);
        }
    })

}

function setPos() {

    $('#way').css({
        clear: 'both',
        height: '600px',
        width: "600px",
        position: 'relative'

    });
    way = $('#way').children();
    var length = way.size();
    $.each(way, function (number, element) {
        var mx = 100;
        var my = 100;
        var radius = 250;
        number = length - number;


        x = radius * Math.cos(number / length * 2 * Math.PI) + mx;
        y = radius * Math.sin(number / length * 2 * Math.PI) + my;
        x = x + 150;
        y = y + ($(this).parent().width() / 2);
        $(element).css({
            position: 'absolute'
        });
        $(element).animate({
            top: x + "px",
            left: y + 'px'
        }, 'slow')
    })
};

//Game options
$("#savebutton").on('click', function(event){
    event.preventDefault();
    $("#optstatusmsg").addClass('hidden');
    var iPlayerCount;
    var iPinCount;
    var iFieldLength;

    //FieldLength
    iFieldLength = $('#fieldlength').val();
    if(iFieldLength < 20){
        $("#optstatusmsg").removeClass('hidden');
        $("#optstatusmsg").html("Field length is to low to play");
    }
    //Player count
    iPlayerCount = $('input[name=numPlayers]:checked').val();
    //Pin count
    iPinCount = $('input[name=numPins]:checked').val();

    //send to server
    json = [
        {
            "action": "options",
            "iPlayerCount": iPlayerCount,
            "iPinCount": iPinCount,
            "iFieldLength": iFieldLength
        }
    ];
    console.log(json);
    json = JSON.stringify(json)
    $.ajax({
        type: "POST",
        dataType: "json",
        data: json,
        contentType: "application/json; charset=utf-8",
        url: "/postjson",
        success: function(msg){
            $('#modalOptions').modal('hide');
            document.location.reload(true);
        },
        failure: function (errmsg) {
            $("#optstatusmsg").html(JSON.parse(errmsg.responseText).message);
        },
        error: function (errmsg) {
            $("#optstatusmsg").html(JSON.parse(errmsg.responseText).message);
        }
    })
});